# yarn-flatpak-demo

Demo yarn/Node app with Flatpak packaging, following the requirements of Flathub publishing.

```shell
$ flatpak run com.demo.yarn-flatpak-demo Olof
Hello Olof
120000 milliseconds as human-readable-value: 2 minutes
```

## dependencies

A Flathub release must specify all its build dependencies, and may not use the `--share=network` flag.

Instead, a fixed list of dependencies is used. To generate these, Flathub provides some python scripts.

## node dependencies

For yarn, [flatpak-node-generator](https://github.com/flatpak/flatpak-builder-tools/blob/master/node/README.md) is
provided.

## local

```shell
git submodule update --init --recursive
nvm use
yarn install --frozen-lockfile
```

## flatpak

Build and install flatpak app
```shell
make install
```

Run
```shell
make run
flatpak run com.demo.yarn-flatpak-demo Olof
```

Uninstall
```shell
make uninstall
```