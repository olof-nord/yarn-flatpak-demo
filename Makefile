SHELL := /bin/bash

FLATPAK := $(shell which flatpak)
FLATPAK_BUILDER := $(shell which flatpak-builder)
FLATPAK_NODE_GENERATOR := $(shell which flatpak-node-generator)

FLATPAK_MANIFEST = com.demo.yarn-flatpak-demo.yml
FLATPAK_APPID = com.demo.yarn-flatpak-demo

FLATPAK_BUILD_FLAGS := --verbose --force-clean --install-deps-from=flathub --ccache
FLATPAK_INSTALL_FLAGS := --verbose --force-clean --ccache --user --install
FLATPAK_DEBUG_FLAGS := --verbose --run

all: build

.PHONY: generate-dependencies
generate-dependencies:
	$(FLATPAK_NODE_GENERATOR) yarn yarn.lock \
		--output generated-sources.json

.PHONY: build
build:
	$(FLATPAK_BUILDER) build $(FLATPAK_BUILD_FLAGS) $(FLATPAK_MANIFEST)

.PHONY: debug
debug:
	$(FLATPAK_BUILDER) $(FLATPAK_DEBUG_FLAGS) build $(FLATPAK_MANIFEST) sh

.PHONY: debug-installed
debug-installed:
	$(FLATPAK) run --command=sh --devel $(FLATPAK_APPID)

.PHONY: install
install:
	$(FLATPAK_BUILDER) build $(FLATPAK_INSTALL_FLAGS) $(FLATPAK_MANIFEST)

.PHONY: uninstall
uninstall:
	$(FLATPAK) uninstall $(FLATPAK_APPID)

.PHONY: run
run:
	$(FLATPAK) run $(FLATPAK_APPID) "Olof"

.PHONY: update-submodules
update-submodules:
	git submodule foreach git pull

.PHONY: install-flatpak-node-generator
install-flatpak-node-generator:
	pipx install --force ./flatpak-builder-tools/node