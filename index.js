"use strict";

const humanizeDuration = require("humanize-duration");

const myArg = process.argv.slice(2);
console.log(`Hello ${myArg}`);

const milliseconds = 120000;
console.log(`${milliseconds} milliseconds as human-readable-value: ${humanizeDuration(milliseconds)}`);
